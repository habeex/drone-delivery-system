package com.example.drones.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ServerResponse {
    private String message;
    private Object data;

    public ServerResponse() {
    }

    public ServerResponse(String message, Object data) {
        this.message = message;
        this.data = data;
    }

}
