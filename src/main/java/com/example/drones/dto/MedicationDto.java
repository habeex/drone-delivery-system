package com.example.drones.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;

/**
 * This is a DTO class that represents a Medication.
 * @author Olorunishola Habeeb
 *
 */
@Data
@Valid
public class MedicationDto {
    @NotNull(message = "Name is required")
    @NotEmpty(message = "Name is required")
    @Pattern(regexp = "^[a-zA-Z0-9_-]+$", message = "Name only accept alphanumeric characters, underscores, and hyphens")
    private String name; // The name of the medication and it must contain only alphanumeric characters, underscores, and hyphens.
    @NotNull(message = "Weight can not be null")
    private double weight; // The weight of the medication
    @NotNull(message = "Code is required")
    @NotEmpty(message = "Code is required")
    @Pattern(regexp = "^[A-Z0-9_]+$", message = "Code only accept letters, numbers, and underscores")
    private String code; // The code of the medication and should contain only letters, numbers, and underscores.
    private String image; // The image of the medication
}
