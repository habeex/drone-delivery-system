package com.example.drones.dto;

import com.example.drones.enumType.DroneModel;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * This is a DTO class that represents a Drone.
 * @author Olorunishola Habeeb
 *
 */
@Data
public class DroneDto {
    @NotEmpty(message = "Serial number is required")
    @NotNull(message = "Serial number is required")
    private String serialNumber;  // The serial number of the drone
    @NotNull(message = "model is required")
    private DroneModel model;     // The model of the drone
    @Max(value = 500, message = "Weight limit is min of 1gr and max of 500gr")
    @Min(value = 1, message = "Weight limit is min of 1gr and max of 500gr")
    private double weightLimit;   // The weight limit of the drone
    @Max(value = 100, message = "Battery capacity requires between 0 and 100%")
    private int batteryCapacity;  // The battery capacity of the drone
}
