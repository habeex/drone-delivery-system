package com.example.drones.service;

import com.example.drones.model.BatteryLog;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public interface BatteryLogService {

    List<BatteryLog> getAllBatteryLogs();

    List<BatteryLog> getBatteryLogsByDroneSerialNumber(String serialNumber);

    List<BatteryLog> getBatteryLogsByDateRange(LocalDateTime startDate, LocalDateTime endDate);

    List<BatteryLog> getBatteryLogsByDroneSerialNumberAndTimestampBetween(String serialNumber, LocalDateTime startDate, LocalDateTime endDate);

}
