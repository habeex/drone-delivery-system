package com.example.drones.service;

import com.example.drones.dto.DroneDto;
import com.example.drones.dto.MedicationDto;
import com.example.drones.enumType.DroneState;
import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import com.example.drones.repository.DroneRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Transactional
@Service
public class DroneServiceImpl implements DroneService {
    private final DroneRepository droneRepository;

    private final ObjectMapper mapper;

    /**
     * Register a new drone in the system with droneDro object.
     *
     * This method takes in a DroneDTO object and registers a new drone in the system with the given DroneDTO data.
     * It maps the DroneDTO object to a Drone object, sets its state to IDLE, saves it to the repository
     * and returns the saved Drone object.
     *
     * @param droneDto a DTO object containing drone data
     * @return the Drone object that is saved in the repository
     */
    @Override
    public Drone registerDrone(DroneDto droneDto) {
        Drone drone = mapper.convertValue(droneDto, Drone.class);
        drone.setState(DroneState.IDLE);
        Drone savedDrone = droneRepository.save(drone);
        return savedDrone;
    }

    /**
     * Loads the given medicines to the drone of a given serial number.
     *
     * @param droneSerialNumber the serial number of the drone.
     * @param medications List of medicationDto objects.
     * @throws IllegalArgumentException if the drone with the given serial number is not found.
     */
    @Override
    public void loadDrone(String droneSerialNumber, List<MedicationDto> medications) {
        Drone drone = getDroneBySerialNumber(droneSerialNumber);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with serial number " + droneSerialNumber + " not found");
        }
        loadDrone(medications, drone);
    }

    /**
     * Loads the given medicines to the drone of a given id.
     *
     * @param id the serial number of the drone.
     * @param medications List of medicationDto objects.
     * @throws IllegalArgumentException if the drone with the given serial number is not found.
     */
    @Override
    public void loadDrone(Long id, List<MedicationDto> medications) {
        Drone drone = getDroneById(id);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with id " + id + " not found");
        }
        loadDrone(medications, drone);
    }

    /**
     * Loads medications for the specified drone.
     *
     * @param medicationDtos List of {@link MedicationDto} to be loaded in the drone
     * @param drone The {@link Drone} to be loaded
     * @throws IllegalStateException if the drone is not in IDLE state
     * @throws IllegalArgumentException if the total weight of medications exceeds the drone weight limit
     * @throws IllegalStateException if the drone battery level is below 25%
     */
    private void loadDrone(List<MedicationDto> medicationDtos, Drone drone) {
        if (medicationDtos.size() < 1) {
            throw new IllegalArgumentException("Please add medications");
        }
        if (drone.getState() != DroneState.IDLE) {
            throw new IllegalStateException("Drone is not in IDLE state");
        }
        double totalWeight = medicationDtos.stream().mapToDouble(MedicationDto::getWeight).sum();
        if (totalWeight > drone.getWeightLimit()) {
            throw new IllegalArgumentException("Total weight of loaded medications exceeds drone weight limit");
        }
        int batteryLevel = drone.getBatteryCapacity();
        if (batteryLevel < 25) {
            throw new IllegalStateException("Drone battery level is below 25%");
        }
        List<Medication> medications = mapper.convertValue(medicationDtos,  new TypeReference<List<Medication>>(){});
        drone.setMedications(medications);
        drone.setState(DroneState.LOADED);
        droneRepository.save(drone);
    }

    /**
     * This method returns a list of available drones from the drone repository by their state.
     * @return a list of drones with the DroneState 'IDLE'.
     */
    @Override
    public List<Drone> getAvailableDrones() {
        return droneRepository.findByState(DroneState.IDLE);
    }

    /**
     * Gets the battery level of a drone given its serial number.
     *
     * @param droneSerialNumber The serial number of the drone
     * @return The battery level of the drone
     * @throws IllegalArgumentException if the drone with the given serial number is not found
     */
    @Override
    public int getDroneBatteryLevel(String droneSerialNumber) {
        Drone drone = getDroneBySerialNumber(droneSerialNumber);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with serial number " + droneSerialNumber + " not found");
        }
        return drone.getBatteryCapacity();
    }

    /**
     * Retrieves the battery level of a drone identified by its id.
     *
     * @param id The id of the drone to get the battery level from.
     * @return The battery level of the drone.
     * @throws IllegalArgumentException if the drone with the given id cannot be found.
     */
    @Override
    public int getDroneBatteryLevel(Long id) {
        Drone drone = getDroneById(id);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with id " + id + " not found");
        }
        return drone.getBatteryCapacity();
    }


    /**
     * This method gets the list of medications loaded in the drone with the given serial number. If the drone is not found, it throws an IllegalArgumentException.
     **/
    @Override
    public List<Medication> getLoadedMedications(String droneSerialNumber) {
        Drone drone = getDroneBySerialNumber(droneSerialNumber);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with serial number " + droneSerialNumber + " not found");
        }
        return drone.getMedications();
    }

    /**
     * This method gets the list of medications loaded in the drone with the given id. If the drone is not found, it throws an IllegalArgumentException.
     **/
    @Override
    public List<Medication> getLoadedMedications(Long id) {
        Drone drone = getDroneById(id);
        if (drone == null) {
            throw new IllegalArgumentException("Drone with id " + id + " not found");
        }
        return drone.getMedications();
    }

    /**
     * Retrieves a Drone from the repository based on its serial number.
     *
     * @param serialNumber The serial number of the Drone
     * @return The Drone with the given serial number, or null if no Drone with that serial number exists in the repository
     */
    private Drone getDroneBySerialNumber(String serialNumber) {
        Optional<Drone> droneOptional = droneRepository.findBySerialNumber(serialNumber);
        return droneOptional.orElse(null);
    }

    /**
     * Retrieves a Drone from the repository based on its serial number.
     *
     * @param id The Id of the Drone
     * @return The Drone with the id, or null if no Drone with that id exists in the repository
     */
    private Drone getDroneById(Long id) {
        Optional<Drone> droneOptional = droneRepository.findById(id);
        return droneOptional.orElse(null);
    }
}
