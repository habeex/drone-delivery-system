package com.example.drones.service;

import com.example.drones.dto.DroneDto;
import com.example.drones.dto.MedicationDto;
import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This interface provides a set of methods for managing drones
 * and the medications they carry. It enables registering
 * of drones as well as loading and retrieving medicines on them.
 * It also provides a list of available drones and their battery levels.
 **/
@Service
public interface DroneService {
    Drone registerDrone(DroneDto drone);
    void loadDrone(String droneSerialNumber, List<MedicationDto> medications);
    void loadDrone(Long id, List<MedicationDto> medications);
    List<Drone> getAvailableDrones();
    int getDroneBatteryLevel(String droneSerialNumber);
    int getDroneBatteryLevel(Long id);
    List<Medication> getLoadedMedications(String droneSerialNumber);
    List<Medication> getLoadedMedications(Long id);
}
