package com.example.drones.service;

import com.example.drones.model.BatteryLog;
import com.example.drones.repository.BatteryLogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class BatteryLogServiceImpl implements BatteryLogService{

    private final BatteryLogRepository batteryLogRepository;

    @Override
    public List<BatteryLog> getAllBatteryLogs() {
        return batteryLogRepository.findAll();
    }

    @Override
    public List<BatteryLog> getBatteryLogsByDroneSerialNumber(String serialNumber) {
        return batteryLogRepository.findByDroneSerialNumber(serialNumber);
    }

    @Override
    public List<BatteryLog> getBatteryLogsByDateRange(LocalDateTime startDate, LocalDateTime endDate) {
        return batteryLogRepository.findByTimestampBetween(startDate, endDate);
    }

    @Override
    public List<BatteryLog> getBatteryLogsByDroneSerialNumberAndTimestampBetween(String serialNumber, LocalDateTime startDate, LocalDateTime endDate) {
        return batteryLogRepository.findByDroneSerialNumberAndTimestampBetween(serialNumber, startDate, endDate);
    }
}
