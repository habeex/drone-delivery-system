package com.example.drones.controller;

import com.example.drones.dto.ServerResponse;
import com.example.drones.model.BatteryLog;
import com.example.drones.service.BatteryLogService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/battery-log")
@RequiredArgsConstructor
public class BatteryLogController {

    private final BatteryLogService batteryLogService;

    /**
     * This endpoint is used to get all the battery logs
     * @return a ServerResponse object containing a success message and list of battery logs
     */
    @Operation(summary = "Get all the entire battery logs")
    @GetMapping
    public ResponseEntity<ServerResponse> getAllBatteryLogs() {
        List<BatteryLog> batteryLogs = batteryLogService.getAllBatteryLogs();
        return ResponseEntity.ok(new ServerResponse("Success", batteryLogs));
    }

    /**
     * This endpoint allows users to filter battery logs by drone serial number. It takes the serial number as a path parameter and returns a list of BatteryLog objects.
     *
     * @return a ServerResponse object containing a success message and list of battery logs
     */
    @Operation(summary = "Filter the battery logs by drone serial number")
    @GetMapping("/drone/{serialNumber}")
    public ResponseEntity<ServerResponse> getBatteryLogsByDroneSerialNumber(@PathVariable String serialNumber) {
        List<BatteryLog> batteryLogs = batteryLogService.getBatteryLogsByDroneSerialNumber(serialNumber);
        return ResponseEntity.ok(new ServerResponse("Success", batteryLogs));
    }

    /**
     * This endpoint allows users to filter battery logs by date range. It takes date range in this format yyyy-MM-dd and returns a list of BatteryLog objects.
     *
     * @return a ServerResponse object containing a success message and list of battery logs
     */
    @Operation(summary = "Filter the battery logs by date range")
    @GetMapping("/date-range")
    public ResponseEntity<ServerResponse> getBatteryLogsByDateRange(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate startDate,
                                                                    @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate endDate) {
        List<BatteryLog> batteryLogs = batteryLogService.getBatteryLogsByDateRange(LocalDateTime.of(startDate, LocalTime.now()), LocalDateTime.of(endDate, LocalTime.now()));
        return ResponseEntity.ok(new ServerResponse("Success", batteryLogs));
    }

    /**
     * This endpoint is used to filter the battery logs by drone serial number and by date range.
     *
     * @return a ServerResponse object containing a success message and list of battery logs
     */
    @Operation(summary = "Filter the battery logs by drone serial number and by date range")
    @GetMapping("/drone/{serialNumber}/date-range")
    public ResponseEntity<ServerResponse> getBatteryLogsByDroneSerialNumberAndTimestampBetween(@PathVariable String serialNumber, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate startDate,
                                                                      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-dd") LocalDate endDate) {
        List<BatteryLog> batteryLogs = batteryLogService.getBatteryLogsByDroneSerialNumberAndTimestampBetween(serialNumber, LocalDateTime.of(startDate, LocalTime.now()), LocalDateTime.of(endDate, LocalTime.now()));
        return ResponseEntity.ok(new ServerResponse("Success", batteryLogs));
    }
}

