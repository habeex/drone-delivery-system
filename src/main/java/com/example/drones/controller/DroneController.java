package com.example.drones.controller;

import com.example.drones.dto.DroneDto;
import com.example.drones.dto.MedicationDto;
import com.example.drones.dto.ServerResponse;
import com.example.drones.dto.ValidList;
import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import com.example.drones.service.DroneService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
* DroneController handling the Drone management related operations
* */
@RequiredArgsConstructor
@RestController
@RequestMapping("/drones")
@Validated
public class DroneController {

    private final DroneService droneService;

    /**
     * Register a new drone
     * @param drone the drone to register
     * @return the registered drone
     */
    @Operation(summary = "Register drone")
    @PostMapping
    public ResponseEntity<ServerResponse> registerDrone(@Valid @RequestBody DroneDto drone) {
        Drone savedDrone = droneService.registerDrone(drone);
        return new ResponseEntity<>(new ServerResponse("Created successfully", savedDrone), HttpStatus.CREATED);
    }

    /**
     * Load a drone with medications
     * @param serialNumber the serial number of the drone
     * @param medications the medications to load
     * @return a response indicating success or failure
     */
    @Operation(summary = "Load drone with medication using drone serialNumber")
    @PostMapping("{serialNumber}/load")
    public ResponseEntity<ServerResponse> loadDrone(@PathVariable("serialNumber") String serialNumber, @RequestBody List<@Valid MedicationDto> medications) {
        droneService.loadDrone(serialNumber, medications);
        return new ResponseEntity<>(new ServerResponse("Drone loaded successfully", null), HttpStatus.OK);
    }

    /**
     * Load a drone with medications
     * @param serialNumber the serial number of the drone
     * @param droneId the id of the drone
     * @param medications the medications to load
     * @return a response indicating success or failure
     */
    @Operation(summary = "Load drone with medication using either drone serialNumber or id")
    @PostMapping("/load")
    public ResponseEntity<ServerResponse> loadDrone(@RequestParam(value = "serialNumber", required = false) String serialNumber, @RequestParam(value = "droneId", required = false) Long droneId, @RequestBody @Valid ValidList<MedicationDto> medications) {
        if(serialNumber != null){
            droneService.loadDrone(serialNumber, medications);
        }else if ((droneId != null)){
            droneService.loadDrone(droneId, medications);
        }else {
            throw new IllegalStateException("Either drone serialNumber or id is required as a request param");
        }
        return new ResponseEntity<>(new ServerResponse("Drone loaded successfully", null), HttpStatus.OK);
    }

    /**
     * Get the list of medications loaded on a drone
     * @param droneId the id of the drone
     * @return the list of medications
     */
    @Operation(summary = "get Loaded medication for a drone using drone id")
    @GetMapping("id/{droneId}/medications")
    public ResponseEntity<ServerResponse> getLoadedMedications(@PathVariable Long droneId) {
        List<Medication> medications = droneService.getLoadedMedications(droneId);
        return new ResponseEntity<>(new ServerResponse("Successfully", medications), HttpStatus.OK);
    }

    /**
     * Get the list of medications loaded on a drone
     * @param serialNumber the serial number of the drone
     * @return the list of medications
     */
    @Operation(summary = "get Loaded medication for a drone using drone serial number")
    @GetMapping("serialNumber/{serialNumber}/medications")
    public ResponseEntity<ServerResponse> getLoadedMedications(@PathVariable String serialNumber) {
        List<Medication> medications = droneService.getLoadedMedications(serialNumber);
        return new ResponseEntity<>(new ServerResponse("Successfully", medications), HttpStatus.OK);
    }

    /**
     * Get the list of available drones
     * @return the list of available drones
     */
    @Operation(summary = "Get available drones")
    @GetMapping("/available")
    public ResponseEntity<ServerResponse> getAvailableDrones() {
        List<Drone> drones = droneService.getAvailableDrones();
        return new ResponseEntity<>(new ServerResponse("Successfully", drones), HttpStatus.OK);
    }

    /**
     * Get the battery level of a drone
     * @param droneId the id of the drone
     * @return the battery level
     */
    @Operation(summary = "Get drone battery level using drone id")
    @GetMapping("id/{droneId}/battery-level")
    public ResponseEntity<ServerResponse> getDroneBatteryLevel(@PathVariable Long droneId) {
        Integer batteryLevel = droneService.getDroneBatteryLevel(droneId);
        return new ResponseEntity<>(new ServerResponse("Successfully", batteryLevel), HttpStatus.OK);
    }

    /**
     * Get the battery level of a drone
     * @param serialNumber the serial number of the drone
     * @return the battery level
     */
    @Operation(summary = "Get drone battery level using serial number")
    @GetMapping("serialNumber/{serialNumber}/battery-level")
    public ResponseEntity<ServerResponse> getDroneBatteryLevel(@PathVariable String serialNumber) {
        Integer batteryLevel = droneService.getDroneBatteryLevel(serialNumber);
        return new ResponseEntity<>(new ServerResponse("Successfully", batteryLevel), HttpStatus.OK);
    }
}

