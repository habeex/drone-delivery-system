package com.example.drones.scheduller;

import com.example.drones.enumType.DroneState;
import com.example.drones.model.BatteryLog;
import com.example.drones.model.Drone;
import com.example.drones.repository.BatteryLogRepository;
import com.example.drones.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DroneBatteryChecker {

    private static final Logger logger = LoggerFactory.getLogger(DroneBatteryChecker.class);
    private final DroneRepository droneRepository;
    private final  BatteryLogRepository batteryLogRepository;

    // This method execute every minute to check the battery level of each drone in the repository.
    // If the level is below 25% a warning will be logged and saved in the batteryLogRepository.
    @Scheduled(fixedDelay = 60000) // check battery level every minute
    public void checkBatteryLevel() {
        List<Drone> drones = droneRepository.findAll();
        for (Drone drone : drones) {
            if (drone.getState() == DroneState.IDLE || drone.getState() == DroneState.RETURNING) {
                int batteryLevel = drone.getBatteryCapacity();
                if (batteryLevel < 25) {
                    logger.warn("Drone {} has low battery level ({}) - unable to dispatch", drone.getSerialNumber(), batteryLevel);
                }
                // create battery log
                BatteryLog batteryLog = new BatteryLog(drone, batteryLevel);
                batteryLogRepository.save(batteryLog);
            }
        }
    }
}

