package com.example.drones.enumType;

public enum DroneModel { Lightweight, Middleweight, Cruiserweight, Heavyweight}
