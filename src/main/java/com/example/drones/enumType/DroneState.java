package com.example.drones.enumType;

public enum DroneState {IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING}
