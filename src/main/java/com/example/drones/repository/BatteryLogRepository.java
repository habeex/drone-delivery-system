package com.example.drones.repository;

import com.example.drones.model.BatteryLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface BatteryLogRepository extends JpaRepository<BatteryLog, Long> {

    List<BatteryLog> findAll();

    List<BatteryLog> findByDroneSerialNumber(String droneSerialNumber);

    List<BatteryLog> findByTimestampBetween(LocalDateTime startDate, LocalDateTime endDate);

    List<BatteryLog> findByDroneSerialNumberAndTimestampBetween(String droneSerialNumber, LocalDateTime startDate, LocalDateTime endDate);
}
