package com.example.drones.repository;

import com.example.drones.enumType.DroneState;
import com.example.drones.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
    List<Drone> findByState(DroneState state);
    Optional<Drone> findBySerialNumber(String serialNumber);
}
