package com.example.drones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "t_battery_logs")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BatteryLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Drone drone;
    @Column(name = "battery_level")
    private int batteryLevel;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    public BatteryLog(Drone drone, int batteryLevel) {
        this.drone = drone;
        this.batteryLevel = batteryLevel;
        this.timestamp = LocalDateTime.now();
    }
}
