package com.example.drones.exception_handler;
import com.example.drones.dto.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ErrorHandlingControllerAdvice.class);

    /**
     * This method is an Exception Handler for MethodArgumentNotValidException. It intercepts any such exception thrown in the application and returns a response with an appropriate message.
     *
     * @param exception - MethodArgumentNotValidException with the details of the failed validation.
     * @return ResponseEntity with appropriate message and response status code.
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<Object> handle(MethodArgumentNotValidException exception) {
        logger.error("Invalid Input Data Exception ", exception);
        BindingResult result = exception.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return new ResponseEntity<>(processFieldErrors(fieldErrors), null, BAD_REQUEST);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Object> handle(IllegalArgumentException exception) {
        logger.error("Bad Request Exception ", exception);
        return new ResponseEntity<>(new ServerResponse(exception.getMessage(), null), null, BAD_REQUEST);
    }
   @ExceptionHandler(value = {IllegalStateException.class})
    public ResponseEntity<Object> handle(IllegalStateException exception) {
       logger.error("Forbidden Exception ", exception);
       return new ResponseEntity<>(new ServerResponse(exception.getMessage(), null), null, FORBIDDEN);
    }
   @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handle(Exception exception) {
       logger.error("Internal Server Error Exception ", exception);
       return new ResponseEntity<>(new ServerResponse(exception.getMessage(), null), null, INTERNAL_SERVER_ERROR);
    }

    private ServerResponse processFieldErrors(List<FieldError> fieldErrors) {
        return new ServerResponse(fieldErrors.get(0).getDefaultMessage(), null);
    }

}
