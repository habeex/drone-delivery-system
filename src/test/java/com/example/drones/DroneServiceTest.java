package com.example.drones;

import com.example.drones.dto.DroneDto;
import com.example.drones.dto.MedicationDto;
import com.example.drones.enumType.DroneModel;
import com.example.drones.enumType.DroneState;
import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import com.example.drones.repository.DroneRepository;
import com.example.drones.service.DroneServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DroneServiceTest {
    @Mock
    private DroneRepository droneRepository;

    @InjectMocks
    private DroneServiceImpl droneService;
    @Mock
    private ObjectMapper mapper;

    @Test
    @DisplayName("Test the functionality of registering a drone")
    public void testRegisterDrone() {
        // Create a new drone object
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("12345");
        droneDto.setModel(DroneModel.Lightweight);
        droneDto.setBatteryCapacity(100);
        droneDto.setWeightLimit(400);

        // Set the expected value for the saved drone object
        Drone expectedDrone = new Drone();
        expectedDrone.setId(1L);
        expectedDrone.setSerialNumber("12345");
        expectedDrone.setModel(DroneModel.Lightweight);
        expectedDrone.setState(DroneState.IDLE);
        expectedDrone.setBatteryCapacity(100);
        expectedDrone.setWeightLimit(400);

        //Mockito is used to mock the mapper object and the convertValue method is called to convert the droneDto object to the Drone type, expecting the Drone instance expectedDrone.
        Mockito.when(mapper.convertValue(droneDto, Drone.class)).thenReturn(expectedDrone);
        Drone drone = mapper.convertValue(droneDto, Drone.class);

        // Mock the save method of the drone repository to return the expected drone object
        when(droneRepository.save(drone)).thenReturn(expectedDrone);

        // Call the registerDrone method of the drone service
        Drone savedDrone = droneService.registerDrone(droneDto);

        // Verify that the save method of the drone repository was called once
        verify(droneRepository, times(1)).save(drone);

        // Assert that the returned drone object is equal to the expected drone object
        assertEquals(expectedDrone, savedDrone);
        assertNotNull(savedDrone.getId());
        assertEquals(droneDto.getSerialNumber(), savedDrone.getSerialNumber());
        assertEquals(droneDto.getModel(), savedDrone.getModel());
        assertEquals(droneDto.getWeightLimit(), savedDrone.getWeightLimit());
        assertEquals(droneDto.getBatteryCapacity(), savedDrone.getBatteryCapacity());
        assertEquals(expectedDrone.getState(), savedDrone.getState());
    }

    @Test
    @DisplayName("Test to ensure that drone is loaded with medications")
    public void testLoadDroneWithValidMedication() {
        // Create a new drone object
        Drone drone = new Drone();
        drone.setId(1L);
        drone.setSerialNumber("12345");
        drone.setModel(DroneModel.Lightweight);
        drone.setState(DroneState.IDLE);
        drone.setBatteryCapacity(100);
        drone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findById(1L)).thenReturn(Optional.of(drone));

        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setName("Med1");
        medicationDto.setWeight(200);
        medicationDto.setCode("CODE1");
        medicationDto.setImage("image1");

        // Create a list of medication objects
        List<MedicationDto> medicationDtos = new ArrayList<>();
        medicationDtos.add(medicationDto);

        Medication medication = new Medication();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        // Mapped the List of Medication objects from a list of MedicationDtos using Jackson's ObjectMapper.
        List<Medication> medications = mapper.convertValue(medicationDtos, new TypeReference<List<Medication>>(){});
        drone.setMedications(medications);

        when(droneRepository.save(drone)).thenReturn(drone);

        // Call the loadDrone method of the drone service
        droneService.loadDrone(drone.getId(), medicationDtos);

        verify(droneRepository, times(1)).findById(drone.getId());
        verify(droneRepository, times(1)).save(drone);
        assertEquals(DroneState.LOADED, drone.getState());
        assertEquals(medications, drone.getMedications());
    }

    @DisplayName("Test to ensure that drone is only loaded with medications when is available")
    @Test(expected = IllegalStateException.class)
    public void testLoadDroneNotInIdleState() {
        // Create a new drone object
        Drone drone = new Drone();
        drone.setId(1L);
        drone.setSerialNumber("12345");
        drone.setModel(DroneModel.Lightweight);
        drone.setState(DroneState.LOADING);
        drone.setBatteryCapacity(100);
        drone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findById(1L)).thenReturn(Optional.of(drone));

        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setName("Med1");
        medicationDto.setWeight(200);
        medicationDto.setCode("CODE1");
        medicationDto.setImage("image1");

        // Create a list of medication objects
        List<MedicationDto> medicationDtos = new ArrayList<>();
        medicationDtos.add(medicationDto);

        Medication medication = new Medication();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        // Mapped the List of Medication objects from a list of MedicationDtos using Jackson's ObjectMapper.
        List<Medication> medications = mapper.convertValue(medicationDtos, new TypeReference<List<Medication>>(){});
        drone.setMedications(medications);

        // Call the loadDrone method of the drone service
        droneService.loadDrone(drone.getId(), medicationDtos);
    }

    @Test(expected = IllegalStateException.class)
    @DisplayName("Test to ensure that drone is only loaded with medications when the battery level is => 25% capacity using drone id")
    public void testLoadDroneBatteryLevelBelow25PercentByDroneId() {
        // Create the expected value for the drone object when findById
        Drone expectedDrone = new Drone();
        expectedDrone.setId(1L);
        expectedDrone.setSerialNumber("12345");
        expectedDrone.setModel(DroneModel.Lightweight);
        expectedDrone.setState(DroneState.IDLE);
        expectedDrone.setBatteryCapacity(20);
        expectedDrone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findById(expectedDrone.getId())).thenReturn(Optional.of(expectedDrone));

        MedicationDto medication = new MedicationDto();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        // Create a list of medication objects
        List<MedicationDto> medicationDtos = new ArrayList<>();
        medicationDtos.add(medication);

        // Call the loadDrone method of the drone service
        droneService.loadDrone(expectedDrone.getId(), medicationDtos);
    }

    @Test(expected = IllegalStateException.class)
    @DisplayName("Test to ensure that drone is only loaded with medications when the battery level is => 25% capacity using drone serial number")
    public void testLoadDroneBatteryLevelBelow25PercentByDroneSerialNumber() {
        // Create the expected value for the drone object when findById
        Drone expectedDrone = new Drone();
        expectedDrone.setId(1L);
        expectedDrone.setSerialNumber("12345");
        expectedDrone.setModel(DroneModel.Lightweight);
        expectedDrone.setState(DroneState.IDLE);
        expectedDrone.setBatteryCapacity(20);
        expectedDrone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findBySerialNumber(expectedDrone.getSerialNumber())).thenReturn(Optional.of(expectedDrone));

        MedicationDto medication = new MedicationDto();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        // Create a list of medication objects
        List<MedicationDto> medications= new ArrayList<>();
        medications.add(medication);

        // Call the loadDrone method of the drone service
        droneService.loadDrone(expectedDrone.getSerialNumber(), medications);
    }

    @Test(expected = IllegalStateException.class)
    @DisplayName("Test to ensure that drone is not exceeded the weight limit it can carry using drone id")
    public void testLoadDroneExceedsWeightLimitByDroneId() {
        // Create the expected value for the drone object when findById
        Drone expectedDrone = new Drone();
        expectedDrone.setId(1L);
        expectedDrone.setSerialNumber("12345");
        expectedDrone.setModel(DroneModel.Lightweight);
        expectedDrone.setState(DroneState.IDLE);
        expectedDrone.setBatteryCapacity(20);
        expectedDrone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findById(expectedDrone.getId())).thenReturn(Optional.of(expectedDrone));

        // Create a list of medication objects that exceeds the weight limit of the drone
        MedicationDto medication = new MedicationDto();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        MedicationDto medication2 = new MedicationDto();
        medication2.setName("Med2");
        medication2.setWeight(201);
        medication2.setCode("CODE2");
        medication2.setImage("image2");

        // Create a list of medication objects
        List<MedicationDto> medications = new ArrayList<>();
        medications.add(medication);
        medications.add(medication2);


        // Call the loadDrone method of the drone service
        droneService.loadDrone(expectedDrone.getId(), medications);
    }

    @Test(expected = IllegalStateException.class)
    @DisplayName("Test to ensure that drone is not exceeded the weight limit it can carry using drone serial number")
    public void testLoadDroneExceedsWeightLimitByDroneSerialNumber() {
        // Create the expected value for the drone object when findById
        Drone expectedDrone = new Drone();
        expectedDrone.setId(1L);
        expectedDrone.setSerialNumber("12345");
        expectedDrone.setModel(DroneModel.Lightweight);
        expectedDrone.setState(DroneState.IDLE);
        expectedDrone.setBatteryCapacity(20);
        expectedDrone.setWeightLimit(400);

        // Mock the expected drone of the drone repository to return the expected drone object
        when(droneRepository.findBySerialNumber(expectedDrone.getSerialNumber())).thenReturn(Optional.of(expectedDrone));

        // Create a list of medication objects that exceeds the weight limit of the drone
        MedicationDto medication = new MedicationDto();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        MedicationDto medication2 = new MedicationDto();
        medication2.setName("Med2");
        medication2.setWeight(201);
        medication2.setCode("CODE2");
        medication2.setImage("image2");

        // Create a list of medication objects
        List<MedicationDto> medications = new ArrayList<>();
        medications.add(medication);
        medications.add(medication2);

        // Call the loadDrone method of the drone service
        droneService.loadDrone(expectedDrone.getSerialNumber(), medications);
    }

    @Test
    @DisplayName("Test to get available drones service only get drones in IDLE state")
    public void testGetAvailableDrones() {
        // Create a list of drone objects that return when query droneRepository.findByState
        Drone drone1 = new Drone();
        drone1.setSerialNumber("1234567890");
        drone1.setModel(DroneModel.Lightweight);
        drone1.setState(DroneState.IDLE);
        drone1.setBatteryCapacity(100);
        drone1.setWeightLimit(500);

        Drone drone2 = new Drone();
        drone2.setId(1L);
        drone2.setSerialNumber("0987654321");
        drone2.setModel(DroneModel.Heavyweight);
        drone2.setState(DroneState.IDLE);
        drone2.setBatteryCapacity(75);
        drone2.setWeightLimit(400);
        // Add drones to a list object
        List<Drone> drones = List.of(drone1, drone2);

        // Mock the expected drones of the drone repository to return the expected list of drones
        when(droneRepository.findByState(DroneState.IDLE)).thenReturn(drones);

        // Call the getAvailableDrones method of the drone service
        List<Drone> availableDrones = droneService.getAvailableDrones();

        // Assert that the returned drones list of size 2
        assertEquals(2, availableDrones.size());
    }

    @Test
    @DisplayName("Test to verify the functionality of getting medications for a drone using its serial number")
    public void testGetLoadedMedicationsByDroneSerialNumber() {
        Drone drone = new Drone();
        drone.setSerialNumber("1234567890");
        drone.setModel(DroneModel.Lightweight);
        drone.setState(DroneState.IDLE);
        drone.setBatteryCapacity(100);
        drone.setWeightLimit(500);

        // Create a list of medication objects that exceeds the weight limit of the drone
        Medication medication = new Medication();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        Medication medication2 = new Medication();
        medication2.setName("Med2");
        medication2.setWeight(201);
        medication2.setCode("CODE2");
        medication2.setImage("image2");

        // Create a list of medication objects
        List<Medication> medications = new ArrayList<>();
        medications.add(medication);
        medications.add(medication2);
        drone.setMedications(medications);

        // Mock the expected drones of the drone repository to return the expected list of drones
        when(droneRepository.findBySerialNumber(drone.getSerialNumber())).thenReturn(Optional.of(drone));

        // Call the getLoadedMedications method of the drone service
        List<Medication> medicationList = droneService.getLoadedMedications(drone.getSerialNumber());

        // Assert that the returned drones list of size 2
        assertEquals(2, medicationList.size());
    }

    @Test
    @DisplayName("Test to verify the functionality of getting medications for a drone using its id")
    public void testGetLoadedMedicationsByDroneId() {
        Drone drone = new Drone();
        drone.setId(1L);
        drone.setSerialNumber("1234567890");
        drone.setModel(DroneModel.Lightweight);
        drone.setState(DroneState.IDLE);
        drone.setBatteryCapacity(100);
        drone.setWeightLimit(500);

        // Create a list of medication objects that exceeds the weight limit of the drone
        Medication medication = new Medication();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        Medication medication2 = new Medication();
        medication2.setName("Med2");
        medication2.setWeight(201);
        medication2.setCode("CODE2");
        medication2.setImage("image2");

        // Create a list of medication objects
        List<Medication> medications = new ArrayList<>();
        medications.add(medication);
        medications.add(medication2);
        drone.setMedications(medications);

        // Mock the expected drones of the drone repository to return the expected list of drones
        when(droneRepository.findById(drone.getId())).thenReturn(Optional.of(drone));

        // Call the getLoadedMedications method of the drone service
        List<Medication> medicationList = droneService.getLoadedMedications(drone.getId());

        // Assert that the returned drones list of size 2
        assertEquals(2, medicationList.size());
    }
}
