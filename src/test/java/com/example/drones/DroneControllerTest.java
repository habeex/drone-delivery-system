package com.example.drones;

import com.example.drones.controller.DroneController;
import com.example.drones.dto.DroneDto;
import com.example.drones.dto.MedicationDto;
import com.example.drones.dto.ServerResponse;
import com.example.drones.enumType.DroneModel;
import com.example.drones.service.DroneService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class DroneControllerTest {

    @Mock
    private DroneService droneService;
    @InjectMocks
    private DroneController droneController;
    private MockMvc mockMvc;
    @Mock
    private ObjectMapper mapper;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(droneController).build();
    }

    /**
     * This test case tests if the /drones POST request to register drones is successful.
     * It sets up a dummy DroneDto object with the required attributes and calls the controller to register the drone.
     * It then verifies that the DroneService method was called with the correct arguments.
     * @throws Exception if an error occurs during the test case execution.
     */
    @Test
    @DisplayName("Tests if the /drones POST request to register drones is successful")
    public void testRegisterDrone() throws Exception {
        // Create a list of medication objects that exceeds the weight limit of the drone
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("12345");
        droneDto.setModel(DroneModel.Lightweight);
        droneDto.setBatteryCapacity(100);
        droneDto.setWeightLimit(500);

        // make request to controller
        mockMvc.perform(post("/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(droneDto)))
                .andExpect(status().isCreated());

        // verify DroneService method was called with correct arguments
        verify(droneService, times(1)).registerDrone(eq(droneDto));
    }

    /**
     * This test case verifies that the endpoint /drones returns a Bad Request HTTP status code when attempting to register a new drone that exceeds the weight limit.
     *
     * It creates a DroneDTO object with attributes such as serialNumber, model, batteryCapacity, and weightLimit where the weightLimit exceeds the maximum limit of the drone. Then it makes a post request to the '/drones' endpoint using the mockMvc object with the DroneDTO object in the request body in JSON format. Finally, it ensures that the response status is a Bad Request (400).
     *
     * @throws Exception if any exception occurs during test execution.
     */
    @Test
    @DisplayName("Tests if the /drones POST request to register drones failed")
    public void testRegisterDroneWithExceedsWeightLimit() throws Exception {
        // Create a list of medication objects that exceeds the weight limit of the drone
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("12345");
        droneDto.setModel(DroneModel.Lightweight);
        droneDto.setBatteryCapacity(100);
        droneDto.setWeightLimit(600);

        // make request to controller
        mockMvc.perform(post("/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(droneDto)))
                .andExpect(status().isBadRequest());
    }

    /**
     * This test case tests the registration of a new drone object with a battery capacity that exceeds the manufacturer's specified limit. It creates a new DroneDto object with a battery capacity of 200 and weight limit of 500. The drone object is registered by sending a POST request to the '/drones' endpoint created in the controller. The response to the request is expected to have a status code of 400 (bad request), indicating that the battery capacity exceeds the allowed limit.
     * @throws Exception
     */

    /**
     * This test case verifies that the endpoint /drones returns a Bad Request HTTP status code when attempting to register a new drone that exceeds the battery capacity.
     *
     * It creates a DroneDTO object with attributes such as serialNumber, model, batteryCapacity, and weightLimit where the batteryCapacity exceeds the maximum capacity of the drone. Then it makes a post request to the '/drones' endpoint using the mockMvc object with the DroneDTO object in the request body in JSON format. Finally, it ensures that the response status is a Bad Request (400).
     *
     * @throws Exception if any exception occurs during test execution.
     */
    @Test
    @DisplayName("Register drone with exceeds battery capacity")
    public void testRegisterDroneWithExceedsBatteryCapacity() throws Exception {
        // Create a list of medication objects that exceeds the weight limit of the drone
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("12345");
        droneDto.setModel(DroneModel.Lightweight);
        droneDto.setBatteryCapacity(200);
        droneDto.setWeightLimit(500);

        // make request to controller
        mockMvc.perform(post("/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(droneDto)))
                .andExpect(status().isBadRequest());
    }

    /**
     * This test case checks if a drone is successfully loaded with a list of MedicationDtos containing two items, by calling the loadDrone method from DroneService. The test also verifies that the droneService.loadDrone is called once with the expected parameters.
     *
     * @throws Exception if there is an error in the test.
     */
    @Test
    @DisplayName("Load drone with valid data and checks if a drone is successfully loaded with medications")
    public void testLoadDrone() throws Exception {
        // create test data
        String droneSerialNumber = "ABC123";

        // Create a list of medication objects that exceeds the weight limit of the drone
        MedicationDto medication = new MedicationDto();
        medication.setName("Med1");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");

        MedicationDto medication2 = new MedicationDto();
        medication2.setName("Med2*");
        medication2.setWeight(201);
        medication2.setCode("CODE2");
        medication2.setImage("image2");

        // Create a list of medication objects
        List<MedicationDto> medications = new ArrayList<>();
        medications.add(medication);
        medications.add(medication2);

        // make request to controller
        mockMvc.perform(post("/drones/load")
                        .param("serialNumber", droneSerialNumber)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(medications)))
                .andExpect(status().isOk());

        // verify DroneService method was called with correct arguments
        verify(droneService, times(1)).loadDrone(eq(droneSerialNumber), eq(medications));
    }

    /**
     * Tests the loading drone with unacceptable data.
     * Creates a test data of medication objects that exceeds the weight limit of drone, invokes the controller method,
     * and expects an HTTP response with status code 400 (Bad Request).
     *
     * @throws Exception if any error occurs during the test execution
     */
    @Test
    @DisplayName("Load drone with in-valid data and checks if a drone is fail load medications")
    public void testLoadDroneWithUnAcceptableData() throws Exception {
        // create test data
        Long id = 1L;

        // Create a list of medication objects that exceeds the weight limit of the drone
        MedicationDto medication = new MedicationDto();
        medication.setName("Med1*");
        medication.setWeight(200);
        medication.setCode("CODE1");
        medication.setImage("image1");


        // Create a list of medication objects
        List<MedicationDto> medications = new ArrayList<>();
        medications.add(medication);

        // make request to controller
        mockMvc.perform(post("/drones/load")
                        .param("id", String.valueOf(id))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(medications)))
                .andExpect(status().isBadRequest());
    }

    /** Test case for checking the available drones.
     * It sends a GET request to the controller endpoint /drones/available and expects the status to be OK.
     * It verifies that the ServerResponse object returned has a message value of "Successfully" and that the data value is not null.
     *
     * @throws Exception if there is an exception during the test execution
     */
    @Test
    @DisplayName("test get all available drones endpoint")
    public void testGetAvailableDrones() throws Exception {
        // make request to controller
        MvcResult requestResult = mockMvc.perform(get("/drones/available")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        ServerResponse response = parseResponse(requestResult, ServerResponse.class);
        assertEquals("Successfully", response.getMessage());
        assertNotNull(response.getData());
    }

    /**
     * This test case tests the get drone battery level by id endpoint.
     * It makes a request to the controller passing the drone id as 1 and content type as JSON. It expects a 200 OK response with a ServerResponse object which should contain the message "Successfully" and non-null data.
     *
     * @throws Exception if any error occurs while testing the api.
     */
    @Test
    @DisplayName("Tests to get drone battery level by id endpoint")
    public void testGetDroneBatteryLevelByDroneId() throws Exception {
        // make request to controller
        MvcResult requestResult = mockMvc.perform(get("/drones/id/1/battery-level")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        ServerResponse updateBookResponse = parseResponse(requestResult, ServerResponse.class);
        assertEquals("Successfully", updateBookResponse.getMessage());
        assertNotNull(updateBookResponse.getData());
    }


    /**
     * This test case test for validating the Get request to retrieve loaded medications details by drone serial number.
     *
     * @throws Exception if there is an exception thrown while making the request.
     */
    @Test
    @DisplayName("Tests request to retrieve loaded medications details by drone serial number endpoint")
    public void testGetLoadedMedicationsByDroneSerialNumber() throws Exception {
        // make request to controller
        MvcResult requestResult = mockMvc.perform(get("/drones/serialNumber/24576543345/medications")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        ServerResponse updateBookResponse = parseResponse(requestResult, ServerResponse.class);
        assertEquals("Successfully", updateBookResponse.getMessage());
        assertNotNull(updateBookResponse.getData());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T parseResponse(MvcResult result, Class<T> responseClass) {
        try {
            String contentAsString = result.getResponse().getContentAsString();
            System.out.println(contentAsString);
            return new ObjectMapper().readValue(contentAsString, responseClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

