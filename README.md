[[_TOC_]]

---

# Drone Delivery System

This project is a Java Spring Boot implementation of a RESTful API for managing a fleet of drones that deliver medications.

### Getting Started
To build and run the project locally, you will need to have the following installed:

* Java 11 or later
* Maven 3.6 or later
* Docker (optional)

To build the project, run the following command in the project root directory:

```
mvn clean install
```

This will run the tests and create an executable JAR file in the target/ directory.

To run the project, you can use the following command:

```
java -jar target/Drones-0.0.1-SNAPSHOT.jar 
```


Alternatively, you can use Docker to run the project:

```
docker build -t drones .
docker run -p 8080:8080 drones
```


This will build a Docker image and run it in a container.

Once you have the application running, test it via the Swagger Documentation at [Swagger Documentation](http://localhost:8082/swagger-ui/index.html)

### API Endpoints

The following API endpoints are available:

`
POST /drones
`

This endpoint registers a new drone with the system. The request body should be a JSON object with the following fields:

* serialNumber: a string with a maximum length of 100 characters.
* model: one of "Lightweight", "Middleweight", "Cruiserweight", or "Heavyweight".
* weightLimit: a decimal number with a maximum value of 500.
* batteryCapacity: an integer representing the battery capacity as a percentage.

Example request body:

```
{
"serialNumber": "ABC123",
"model": "Lightweight",
"weightLimit": 250,
"batteryCapacity": 80
}
```

Response body: 

```
{
  "message": "Created successfully",
  "data": {
    "id": 6,
    "serialNumber": "ABC123",
    "model": "Lightweight",
    "weightLimit": 250,
    "batteryCapacity": 80,
    "state": "IDLE",
    "medications": null
  }
}
```

### Loading a Drone

`
POST /drones/{serialNumber}/load
`

This endpoint loads a drone with medications. The `serialNumber` path parameter should be replaced with the serial number of the drone to be loaded.

`
POST /drones/load
`

You can also loads a drone with medications using this endpoint. By passing either `droneId` or `serialNumber` as a request parameter

The request body should be a JSON array of medication objects, each with the following fields:

* name: a string consisting of letters, numbers, '-', and '_'.
* weight: a decimal number representing the weight of the medication.
* code: a string consisting of uppercase letters, underscores, and numbers.
* image: a string representing the URL of an image of the medication.


Example request body:

```
[
  {
    "name": "Medication1",
    "weight": 25.7,
    "code": "MED1",
    "image": "https://example.com/med1.jpg"
  },
  {
    "name": "Medication2",
    "weight": 200,
    "code": "MED2",
    "image": "https://example.com/med2.jpg"
  }
]
```

Response body:

```
{
  "message": "Drone loaded successfully"
}
```

### Getting Available Drones for Loading
`
GET /drones/available
`

This endpoint retrieves the list of drones that are currently available for loading. The response body is a JSON array of drone objects.

Response body:

```
{
  "message": "Successfully",
  "data": [
    {
      "id": 2,
      "serialNumber": "21313243434349",
      "model": "Lightweight",
      "weightLimit": 500,
      "batteryCapacity": 45,
      "state": "IDLE",
      "medications": []
    },
    {
      "id": 3,
      "serialNumber": "21313243423234349",
      "model": "Lightweight",
      "weightLimit": 200,
      "batteryCapacity": 89,
      "state": "IDLE",
      "medications": []
    }
  ]
}
```

### Getting Loaded Medications for a Given Drone With Drone Serial Number

`
GET /drones/{serialNumber}/medications
`

This endpoint retrieves the list of medications loaded on a given drone. The `serialNumber` path parameter should be replaced with the serial number of the drone.

### Getting Loaded Medications for a Given Drone With Drone Id

`
GET /drones/id/{droneId}/medications
`

This endpoint retrieves the list of medications loaded on a given drone. The `droneId` path parameter should be replaced with the id of the drone.

Response body:

```
{
  "message": "Successfully",
  "data": [
    {
      "id": 2,
      "name": "Medication1",
      "weight": 25.7,
      "code": "MED1",
      "image": "https://example.com/med1.jpg"
    },
    {
      "id": 3,
      "name": "Medication2",
      "weight": 200,
      "code": "MED2",
      "image": "https://example.com/med2.jpg"
    }
  ]
}
```

### Getting Drone Battery Level With Drone Serial Number

`
GET /drones/{serialNumber}/battery-level
`

This endpoint retrieves drone battery level. The `serialNumber` path parameter should be replaced with the serial number of the drone.

### Getting Drone Battery Level With Drone Id

`
GET /drones/id/{droneId}/battery-level
`

This endpoint retrieves drone battery level. The `droneId` path parameter should be replaced with the id of the drone.

Response body:

```
{
  "message": "Successfully",
  "data": 45
}
```

### Drones Battery Levels History/Audit Event Log

To keep track of the battery levels of the drones in the fleet, a periodic task has been implemented in the service. This task runs at regular intervals and checks the battery level of each drone. If the battery level is below a certain threshold, an entry is added to the drones battery levels history/audit event log.

The drones battery levels history/audit event log provides a record of all the battery level checks that have been performed by the service. Each entry in the log includes the following information:

Drone Serial Number
Battery Level
Timestamp
The log is stored in the database and can be accessed via the REST API. Clients can retrieve the entire log or filter it by drone serial number or date range.

To retrieve the entire log, make a GET request to the following endpoint:

`
GET /drones/battery-log
`

To filter the log by drone serial number, make a GET request to the following endpoint:

`
GET /drones/battery-log?serialNumber=<serialNumber>
`

Replace <droneSerialNumber> with the serial number of the drone you want to filter by.


To filter the log by date range, make a GET request to the following endpoint:

`
GET /drones/battery-log?startDate=<startDate>&endDate=<endDate>
`

Replace <startDate> and <endDate> with the start and end dates of the date range you want to filter by, respectively. The dates should be in the format yyyy-MM-dd

To filter the log by drone serial number and date range, make a GET request to the following endpoint:

`
GET /drones/battery-log?serialNumber=<serialNumber>&startDate=<startDate>&endDate=<endDate>
`

Response body:

```
{
  "message": "Success",
  "data": [
    {
      "id": 5,
      "drone": {
        "id": 2,
        "serialNumber": "21313243434349",
        "model": "Lightweight",
        "weightLimit": 600,
        "batteryCapacity": 45,
        "state": "IDLE",
        "medications": []
      },
      "batteryLevel": 45,
      "timestamp": "2023-04-12T00:28:33.884382"
    }
  ]
}
```